const http = require("http");
const readFile = require("fs-readfile-promise");
const path = require("path");

function Server(){

    this.getConfig = async function(){

        let content = await readFile(path.join(__dirname,"files/server.env"));
        let configContent = JSON.parse(content.toString());
        return configContent;
        
    }

    this.runServer = async function(){

        let configData = await this.getConfig();
        
        return http.createServer((req, res) => {
            console.log(req.url);
        }).listen(configData.port, () => console.log(`Servidor corrento em ${configData.port}`));
                
    }

    return this;

}

module.exports = {
    instance: new Server()
};
