const mysql = require("mysql");
const neo4j = require("neo4j-driver");

function Connection(){

    this.mysqlConnect = function(){

        const cx = mysql.createConnection({
            user:"naka",
            password:"1234",
            host:"localhost",
            database:"dummy"
        });
        return cx;
    }

    this.getMysqlConnection = function(){
        return this.mysqlConnect();
    }


    this.neo4jConnect = function(){

        let driver = neo4j.driver(
            'neo4j://localhost',
            neo4j.auth.basic('neo4j', '1234')
        )

        return driver;
    }

    this.getNeo4jConnection = function(){
        return this.neo4jConnect();
    }

    return this;
}


//(connection.instance.getMysqlConnection().query("SELECT * FROM customers",(err, result) => console.log(result)))

//Singleton connectio object
connection = {
    instance: new Connection(),
}

module.exports = {
    neo4jConnection: connection.instance.getNeo4jConnection(),
    mysqlConnection: connection.instance.getMysqlConnection(),
    sqlServerConnection: null,
    oracleConnetion: null,
}
