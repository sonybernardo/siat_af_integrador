const Server = require("./config/server.js");
const {Customer} = require("./bo/Customer");
const {Payment} = require("./bo/Payment.js");
const {Order} = require("./bo/Order.js");
const {Employee} = require("./bo/Employee.js");


const http = require("http");

http.createServer((req, res) => {

    console.log(req.url);

    if(req.url == "/sync"){

        customer = Customer;
        payment = Payment;
        order = Order;
        employee = Employee;
        
        customer.next = Order;
        order.next = Payment;
        payment.next = Employee;
        
        ultimoProcesso = function(){

            customer.getCustomerOrders();
            customer.getCustomerPayments();
            employee.getEmployeeCustomer(() => {

                console.log("Agora terminou");
                res.setHeader("Content-Type", "text/json");
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.end(`{success: true}`);

            });

        };
        
        customer.getAndParseDados(ultimoProcesso);

    }

}).listen(3000, () => console.log("Servidor em execução"))



