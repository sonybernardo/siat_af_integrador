exports.OrderdetailsVO = function OrderdetailsVO(obj){

    let orderDetails = {
        code: obj.orderNumber +""+obj.orderLineNumber,
        label: obj.orderNumber,

        orderNumber: (obj.orderNumber || 0),
        productCode: (obj.productCode || 0),
        quantityOrdered: (obj.quantityOrdered || 0),
        priceEach: (obj.priceEach || 0),
        orderLineNumber: (obj.orderLineNumber || 0),
    }

    return {
        json: orderDetails,
        string: JSON.stringify(orderDetails),
        formated: JSON.stringify(orderDetails).replace(/"([^"]+)":/g, '$1:')
    };

}