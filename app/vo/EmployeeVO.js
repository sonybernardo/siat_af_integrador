
exports.EmployeeVO = function EmployeeVO(obj){

    let employee = {
        
        code: obj.employeeNumber,
        label: `${(obj.firstName || "")} ${(obj.lastName || "")}`,

        employeeNumber: (obj.employeeNumber || 0),
        lastName: (obj.lastName || ""),
        firstName: (obj.firstName || ""),
        extension: (obj.extension || ""),
        email: (obj.email || ""),
        officeCode: (obj.officeCode || 0),
        reportsTo: (obj.reportsTo || 0),
        jobTitle: (obj.jobTitle || "")
    }

    return {
        json: employee, 
        string: JSON.stringify(employee),
        formated : JSON.stringify(employee).replace(/"([^"]+)":/g, '$1:')
    };

}

