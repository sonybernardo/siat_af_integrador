
exports.PaymentVO = function PaymentVO(obj){

    //console.log("Passou");

    let payment = {
        
        code : obj.checkNumber,
        label : obj.checkNumber,

        customerNumber: (obj.customerNumber || 0),
        checkNumber: (obj.checkNumber || ""),
        paymentDate: (obj.paymentDate || ""),
        amount: (obj.amount || 0)
          
    }

    return {
        json: payment, 
        string: JSON.stringify(payment),
        formated : JSON.stringify(payment).replace(/"([^"]+)":/g, '$1:')
    };


}

