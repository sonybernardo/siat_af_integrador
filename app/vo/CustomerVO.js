
exports.CustomerVO = function CustomerVO(obj){

    //console.log("Passou");

    let customer = {

        code: obj.customerNumber,
        label: `${(obj.customerName || "")} `,

        customerNumber: (obj.customerNumber || 0),
        customerName: (obj.customerName || ""),
        contactLastName: (obj.contactLastName || ""),
        contactFirstName: (obj.contactFirstName || ""),
        phone: (obj.phone || ""),
        addressLine1: (obj.addressLine1 || ""),
        addressLine2: (obj.addressLine2 || ""),
        city: (obj.city || ""),
        state: (obj.state || ""),
        postalCode: (obj.postalCode || ""),
        country: (obj.country || ""),
        salesRepEmployeeNumber: (obj.salesRepEmployeeNumber || ""),
        creditLimit: (obj.creditLimit || ""),
        commited: (obj.commited || "")
    }

    return {
            json: customer, 
            string: JSON.stringify(customer),
            formated : JSON.stringify(customer).replace(/"([^"]+)":/g, '$1:')
        };

}

