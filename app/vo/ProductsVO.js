exports.ProductsVO = function ProductsVO(obj){

    let products = {
        code: (obj.productCode || ""),
        label: (obj.productCode || ""),

        productCode: (obj.productCode || ""),
        productName: (obj.productName || ""),
        productLine: (obj.productLine || ""),
        productScale: (obj.productScale || ""),
        productVendor: (obj.productVendor || ""),
        productDescription: (obj.productDescription || ""),
        quantityInStock: (obj.quantityInStock || 0),
        buyPrice: (obj.buyPrice || 0),
        MSRP: (obj.MSRP || 0),
    }

    return {
        json: products,
        string: JSON.stringify(products),
        formated: JSON.stringify(products).replace(/"([^"]+)":/g, '$1:')
    }
}