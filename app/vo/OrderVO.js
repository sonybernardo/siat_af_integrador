
exports.OrderVO = function OrderVO(obj){

    let order = {

        code: obj.orderNumber,
        label: `${(obj.orderNumber || "")} `,

        orderNumber: (obj.orderNumber || ""),
        orderDate: (obj.orderDate || ""),
        requiredDate: (obj.requiredDate || ""),
        shippedDate: (obj.shippedDate || ""),
        status: (obj.status || ""),
        comments: (obj.comments || ""),
        customerNumber: (obj.customerNumber || "")
          
    }

    return {
        json: order, 
        string: JSON.stringify(order),
        formated : JSON.stringify(order).replace(/"([^"]+)":/g, '$1:')
    };

}

