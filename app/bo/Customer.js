const { CustomerVO } = require("../vo/CustomerVO");
const { PaymentVO } = require("../vo/PaymentVO");
const {OrderVO} = require("../vo/OrderVO");
const AbstractBO = require("./AbstractBO").AbstractBO;

class Customer extends AbstractBO{
    
    table = "customers";
    static queryString = null;
    valueObject = CustomerVO;

    async getAndParseDados(callBack){

        await super.getAndCommitDados({lastCallBack: callBack});

    }


    async getCustomerPayments(){

        let queryString = `
                            SELECT 
                                c.*, 
                                p.*, 
                                c.customerNumber as customerCode, 
                                p.checkNumber as paymentCode 
                            FROM customers c 
                            INNER JOIN payments p ON p.customerNumber = c.customerNumber
                          `;
        const params = {
            queryStringParam: queryString,
            callBackParam: Customer.parseAndCommitPayment
        }

        await super.getAndCommitDados(params);
        console.log("Payment Rel Terminou");

    }

    async getCustomerOrders(){

        let queryString = `
                            SELECT 
                                o.*, 
                                o.orderNumber as orderCode, 
                                c.customerNumber as customerCode 
                            FROM customers c 
                            INNER JOIN orders o ON o.customerNumber = c.customerNumber
                          `;
        const params = {
            queryStringParam: queryString,
            callBackParam: Customer.parseAndCommitOrders
        }

        super.getAndCommitDados(params);

    }


    static parseAndCommitPayment(err, dados){

        for(const record of dados){

            const customer = CustomerVO(record).json;
            const payment = PaymentVO(record).json;

            const queryString = `MATCH (c:Customer {code:${customer.code}}), (o:Payment {code:"${payment.code}"}) MERGE (c)-[b:PAGOU]->(o) RETURN *`;
            super.neo4jCommitTransaction(queryString);
            console.log(`Customer(${customer.code}) -> Payment(${payment.code}) `);

        }
    }

    static parseAndCommitOrders(err, dados){

        for(const record of dados){
            
            const customer = CustomerVO(record).json;
            const order = OrderVO(record).json;

            const queryString = `MATCH (c:Customer {code:${customer.code}}), (o:Order {code:${order.code}}) MERGE (c)-[b:FATUROU]->(o) RETURN *`;
            super.neo4jCommitTransaction(queryString);
            console.log(`Customer(${customer.code}) -> Order(${order.code}) `);

        }
        
    }


}


module.exports = {
    Customer: new Customer(),
} 