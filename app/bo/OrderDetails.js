const { AbstractBO } = require("./AbstractBO");
const { ProductsVO } = require("../vo/ProductsVO");
const OrderDetailsVO = require("../vo/OrderdetailsVO").OrderdetailsVO;

class OrderDetails extends AbstractBO {

    table = "orderdetails";
    valueObject = OrderDetailsVO;

    getAndParseDados() {
        super.getAndCommitDados({});
    }

 
}

module.exports = {
    OrderDetails: new OrderDetails()
}



