const { AbstractBO } = require("./AbstractBO");
const { OrderdetailsVO } =  require("../vo/OrderdetailsVO"); 
const ProductsVO = require("../vo/ProductsVO").ProductsVO;

class Products extends AbstractBO {

    table = "products";
    valueObject = ProductsVO;

    getAndParseDados() {
        super.getAndCommitDados({})
    }

    getOrderDetailsProduct() {

        let queryString = `SELECT 
                                p.productName,
                                o.productCode,
                                o.orderLineNumber,
                                o.orderNumber
                            FROM 
                            products p INNER JOIN orderdetails o ON p.productCode = o.productCode 
                        `;
                        this.database.mysqlConnection.query(queryString,Products.parseAndCommitProducts);
    }

    static parseAndCommitProducts(err, dados){

        for(let record of dados) {
            let orderdetails = OrderdetailsVO(record).json;
            let products = ProductsVO(record).json;

            let queryString = `MATCH
                                (o:OrderDetails {code:"${orderdetails.code}"}),
                                (p:Products {code:"${products.code}"}) MERGE (o)-[d:DETALHES]->(p)
                                RETURN *
                                `;
            super.neo4jCommitTransaction(queryString);
                                
        }
        
    }

}

module.exports = {
    Products: new Products()
}
