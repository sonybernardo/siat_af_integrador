const database = require("../config/db");
const { AbstractBO } = require("./AbstractBO");
const { OrderVO } = require("../vo/OrderVO");


class Order extends AbstractBO{

    table = "orders";
    valueObject = OrderVO;

    async getAndParseDados(){

        super.getAndCommitDados({});
        
    }

}

module.exports = {
    Order: new Order(),
}