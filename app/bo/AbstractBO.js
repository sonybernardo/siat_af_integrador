const database = require("../config/db");
const { Customer } = require("./Customer");
const { CustomerVO } = require("../vo/CustomerVO");

exports.AbstractBO = class AbstractBO{

    /** Local (AbstractBO) properties */
    static entity = null;
    static next = null;
    static currentVO = null;
    static lastCallBack = null;

    /** Extending properties */
    static fullCommitCallBack = null;
    database = database;
    next = null;
    table = null;
    valueObject = null;
    fullCommitCallBack = null;

    set next(nextParam){
        this.next = nextParam;
    }

    async getAndCommitDados({callBackParam, queryStringParam, lastCallBack}){

        if(AbstractBO.lastCallBack == null)
            AbstractBO.lastCallBack = lastCallBack;

        AbstractBO.entity = this.__proto__.constructor.name;
        AbstractBO.next = this.next;
        AbstractBO.currentVO = this.valueObject;
        
        let callBack = callBackParam || this.parseAndcommitEntity;

        let queryString = queryStringParam || `SELECT * FROM ${this.table}`;
        await this.database.mysqlConnection.query(queryString,callBack);

    }

    async parseAndcommitEntity(err, dados){

        let x = 0;
        for(let record of dados){

            let object = AbstractBO.currentVO(record).formated;
            await AbstractBO.commitEntity(object,x,AbstractBO.entity);
            x++;
        }
        console.log(`${AbstractBO.entity} Chegou em: `,x);
        //console.log(AbstractBO.next);

        if(AbstractBO.next){
            AbstractBO.next.getAndParseDados();
        }else{
            AbstractBO.lastCallBack();
        }
        
    }

    static async commitEntity(dados,x,entity){
        
        let session = database.neo4jConnection.session();

        let readTns = await session.writeTransaction(async tns => {
            tns.run(`MERGE (e:${entity} ${dados}) RETURN ID(e)`);
            console.log(`${entity} Foi: `,x);
        });
        
    }


    static async neo4jCommitTransaction(query){

        let session = database.neo4jConnection.session();
        
        let readTns = await session.writeTransaction(async tns => {
            //console.log(query);
            tns.run(`${query}`);
            //console.log("Transact:");
        });

        /*
        readTns.then(res => {
            console.log(res.records);
        })
        */
        
    }



}