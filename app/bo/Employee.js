const database = require("../config/db");
const { AbstractBO } = require("./AbstractBO");
const { CustomerVO } = require("../vo/CustomerVO");
const Customer = require("./Customer");
const EmployeeVO = require("../vo/EmployeeVO").EmployeeVO;


class Employee extends AbstractBO{


    table = "employees";
    valueObject = EmployeeVO;
    

    getAndParseDados(){

        super.getAndCommitDados({});    

    }

    getEmployeeCustomer(fullCommitCallBack){
        
        Employee.fullCommitCallBack = fullCommitCallBack;
        
        let queryString = `SELECT 
                                c.customerNumber,
                                e.employeeNumber
                            FROM 
                            customers c INNER JOIN employees e ON c.salesRepEmployeeNumber = e.employeeNumber
                          `;
        this.database.mysqlConnection.query(queryString,Employee.parseAndCommitCustomers);

    }

    static parseAndCommitCustomers(err, dados){

        for(let record of dados){

            let employee = EmployeeVO(record).json;
            let customer = CustomerVO(record).json;

            let queryString = `
                                MATCH 
                                (e:Employee {code:${employee.code}}),
                                (c:Customer {code:${customer.code}}) MERGE (e)-[r:REPRESENTADO]->(c)
                                RETURN *
                              `;
            
            super.neo4jCommitTransaction(queryString);
            console.log(`Employee(${employee.code}) -> Customer(${customer.code}) `);
            
        }

        if(Employee.fullCommitCallBack){
            Employee.fullCommitCallBack();
        }

    }

}

module.exports = {
    Employee: new Employee(),
}