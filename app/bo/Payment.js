const database = require("../config/db");
const { AbstractBO } = require("./AbstractBO");
const PaymentVO = require("../vo/PaymentVO").PaymentVO;

class Payment extends AbstractBO{

    table = "payments";
    valueObject = PaymentVO;

    async getAndParseDados(){

        super.getAndCommitDados({});

    }


}

module.exports = {
    Payment: new Payment(),
}