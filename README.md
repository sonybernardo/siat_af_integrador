# Fraud Detection

### Description <br>
This is a Setup of the Backend for a Fraud Detection platform, this is Built with NodeJS, Neo4J and MySQL as Data sources and Database.

This Backend saves and Manipulates Graphs to allow indentify connections between different entities to support fraud investigation, for this reason, there are some very important JavaScript implementation which allow some Dynamic as well as code execution based on a Decision tree.

The frontend of this application can be found on [this repository](https://gitlab.com/sonybernardo/siatt_af).

Note: Since this is a setup (POC), this was and can be extended/used as fundantions for Bigger project which Fraud Detection is the main purpose
